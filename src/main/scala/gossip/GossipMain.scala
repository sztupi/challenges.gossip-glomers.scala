package gossip

import cats.effect.{IO, IOApp}
import cats.effect.ExitCode

object GossipMain extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = for {
    _ <- IO(println(args))
  } yield ExitCode(0)
}
