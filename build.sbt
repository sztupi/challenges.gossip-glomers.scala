name := "gossip-glomers"
version := "0.1"
scalaVersion := "3.2.2"

libraryDependencies += "org.typelevel" %% "cats-effect" % "3.4.8"
